export default class Cell {
    constructor(x, y, width_height) {
        this.x = x;
        this.y = y;
        this.element = this.generalDiv(width_height);
    }

    generalDiv(width_height) {
        let div = document.createElement('div');
        div.setAttribute('class', 'cell');
        div.style.width = width_height + "px";
        div.style.height = width_height + "px";
        div.style.lineHeight = width_height + "px";
        return div;
    }

    fill(content) {
        this.content = content.icon;
        this.element.innerHTML = content.icon;
        this.element.style.color = content.color;
    }

    decorateWinner(css) {
        this.element.classList.add(css);
    }

    nX() {
        return this.x - 1;
    }

    nY() {
        return this.y;
    }

    sX() {
        return this.x + 1;
    }

    sY() {
        return this.y;
    }

    eX() {
        return this.x;
    }

    eY() {
        return this.y + 1;
    }

    wX() {
        return this.x;
    }

    wY() {
        return this.y - 1;
    }

    neX() {
        return this.x - 1;
    }

    neY() {
        return this.y + 1;
    }

    seX() {
        return this.x + 1;
    }

    seY() {
        return this.y + 1;
    }

    nwX() {
        return this.x - 1;
    }

    nwY() {
        return this.y - 1;
    }

    swX() {
        return this.x + 1;
    }

    swY() {
        return this.y - 1;
    }
}

