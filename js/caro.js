import Cell from './cell.js';

const CELL_WIDTH_HEIGHT = 28;

const X_COUNT = 20;
const Y_COUNT = 20;

const listPlayer = [{icon: "X", color: "#007bff"}, {icon: "O", color: "#dc3545"}]
let listCell = [];
let currentPlayerIndex = -1;
let currentPlayer;
let gameActive = false;

const winningMessage = () => `Player ${currentPlayer.icon} has won!`;
const drawMessage = `Game ended in a draw!`;
const currentPlayerTurn = () => `It's ${currentPlayer.icon}'s turn`;

const gameBoard = document.querySelector('.game--container');


document.querySelector('.game--restart').addEventListener('click', handleRestartGame);

function showMessage(message) {
    document.querySelector(".game--status").innerHTML = message;
}

function countN(cell, count) {
    let x = cell.nX();
    let y = cell.nY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            count++;
            count = countN(nCell, count);
        } else {
            return count;
        }
    }
    return count;
}

function countS(cell, count) {
    let x = cell.sX();
    let y = cell.sY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            count++;
            count = countS(nCell, count);
        } else {
            return count;
        }
    }
    return count;
}

function countE(cell, count) {
    let x = cell.eX();
    let y = cell.eY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            count++;
            count = countE(nCell, count);
        } else {
            return count;
        }
    }
    return count;
}

function countW(cell, count) {
    let x = cell.wX();
    let y = cell.wY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            count++;
            count = countW(nCell, count);
        } else {
            return count;
        }
    }
    return count;
}

function countNE(cell, count) {
    let x = cell.neX();
    let y = cell.neY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            count++;
            count = countNE(nCell, count);
        } else {
            return count;
        }
    }
    return count;
}

function countSE(cell, count) {
    let x = cell.seX();
    let y = cell.seY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            count++;
            count = countSE(nCell, count);
        } else {
            return count;
        }
    }
    return count;
}

function countNW(cell, count) {
    let x = cell.nwX();
    let y = cell.nwY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            count++;
            count = countNW(nCell, count);
        } else {
            return count;
        }
    }
    return count;
}

function countSW(cell, count) {
    let x = cell.swX();
    let y = cell.swY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            count++;
            count = countSW(nCell, count);
        } else {
            return count;
        }
    }
    return count;
}


function handlerClick(cell) {
    function checkWinning() {
        const LENGTH = 5;
        let n = countN(cell, 0);
        let s = countS(cell, 0);
        let e = countE(cell, 0);
        let w = countW(cell, 0);
        let ne = countNE(cell, 0);
        let nw = countNW(cell, 0);
        let se = countSE(cell, 0);
        let sw = countSW(cell, 0);
        if (n + s >= LENGTH - 1) {
            fillWinning(cell, "ns");
            return true;
        }
        if (e + w >= LENGTH - 1) {
            fillWinning(cell, "ew");
            return true;
        }
        if (ne + sw >= LENGTH - 1) {
            fillWinning(cell, "ne-sw");
            return true;
        }
        if (nw + se >= LENGTH - 1) {
            fillWinning(cell, "nw-se");
            return true;
        }
        return false;
    }

    function checkDraw() {
        return false;
    }

    if (gameActive && (cell.content === undefined || cell.content === null)) {
        cell.fill(currentPlayer);
        if (checkWinning(cell)) {
            setTimeout(function () {
                alert(winningMessage());
            }, 1000);
            gameActive = false;
            showMessage(winningMessage());
            return;
        }
        if (checkDraw()) {
            setTimeout(function () {
                alert(drawMessage);
            }, 1000);
            gameActive = false;
            showMessage(drawMessage);
            return;
        }
        nextPlayer();
        showMessage(currentPlayerTurn());
    }
}

for (let x = 0; x < X_COUNT; x++) {
    for (let y = 0; y < Y_COUNT; y++) {
        let cell = new Cell(x, y, CELL_WIDTH_HEIGHT);
        let element = cell.element;
        element.addEventListener('click', () => handlerClick(cell))
        gameBoard.appendChild(element);
        listCell.push(cell);
    }
}

function buildGridTemplateColumns(a, b) {
    return "repeat(" + a + ", " + (b + 2) + "px)";
}

gameBoard.style.gridTemplateColumns = buildGridTemplateColumns(X_COUNT, CELL_WIDTH_HEIGHT);
gameBoard.style.width = ((CELL_WIDTH_HEIGHT + 1) * X_COUNT + 2) + "px";

let startGame = () => {
    gameActive = true;
    nextPlayer();
    showMessage(currentPlayerTurn());
};

function nextPlayer() {
    if (currentPlayerIndex < listPlayer.length - 1) {
        currentPlayerIndex++;
        currentPlayer = listPlayer[currentPlayerIndex];
    } else {
        currentPlayerIndex = 0;
        currentPlayer = listPlayer[currentPlayerIndex];
    }

}

startGame();

function fillN(cell) {
    let x = cell.nX();
    let y = cell.nY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            nCell.decorateWinner("winner-cell");
            fillN(nCell);
        }
    }
}

function fillS(cell) {
    let x = cell.sX();
    let y = cell.sY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            nCell.decorateWinner("winner-cell");
            fillS(nCell);
        }
    }
}

function fillE(cell) {
    let x = cell.eX();
    let y = cell.eY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            nCell.decorateWinner("winner-cell");
            fillE(nCell);
        }
    }
}

function fillW(cell) {
    let x = cell.wX();
    let y = cell.wY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            nCell.decorateWinner("winner-cell");
            fillW(nCell);
        }
    }
}

function fillNE(cell) {
    let x = cell.neX();
    let y = cell.neY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            nCell.decorateWinner("winner-cell");
            fillNE(nCell);
        }
    }
}

function fillSE(cell) {
    let x = cell.seX();
    let y = cell.seY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            nCell.decorateWinner("winner-cell");
            fillSE(nCell);
        }
    }
}

function fillNW(cell) {
    let x = cell.nwX();
    let y = cell.nwY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            nCell.decorateWinner("winner-cell");
            fillNW(nCell);
        }
    }
}

function fillSW(cell) {
    let x = cell.swX();
    let y = cell.swY();
    if (x >= 0 && y >= 0) {
        let nCell = listCell.find(c => c.x === x && c.y === y);
        if (cell.content === nCell.content) {
            nCell.decorateWinner("winner-cell");
            fillSW(nCell);
        }
    }
}

function fillWinning(cell, direction) {
    cell.decorateWinner("winner-cell");
    switch (direction) {
        case "ns":
            fillN(cell);
            fillS(cell);
            break;
        case "ew":
            fillE(cell);
            fillW(cell);
            break;
        case "ne-sw":
            fillNE(cell);
            fillSW(cell);
            break;
        case "nw-se":
            fillNW(cell);
            fillSE(cell);
            break;
        default:
            break;
    }
}

function handleRestartGame() {
    gameActive = true;
    listCell.forEach(c => {
        c.fill({icon: null, color: null});
        c.element.className = "cell";
    });
}
